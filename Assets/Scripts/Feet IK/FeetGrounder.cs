﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetGrounder : MonoBehaviour
{
    #region Variables
    private Vector3 rightFootPosition, leftFootPosition, leftFootIkPosition, rightFootIkPosition;
    private Quaternion leftFootIkRotation, rightFootIkRotation;
    private float lastPelvisPositionY, lastRightFootPositionY, lastLeftFootPositionY;

    [Header("Feet Grounder")]
    public bool enableFeetIk = true;
    [Range(0, 2)]
    [SerializeField]
    private float heightFromGroundRaycast = 1.14f;
    [Range(0, 2)]
    [SerializeField]
    private float raycastDownDistance = 1.5f;
    [SerializeField]
    private LayerMask enviromentLayer;
    [SerializeField]
    private float pelvisOffset = 0f;
    [Range(0, 1)]
    [SerializeField]
    private float pelvisUpAndDownSpeed = 0.28f;
    [Range(0, 1)]
    [SerializeField]
    private float feetToIkPositionSpeed = 0.5f;

    public string leftFootAnimVariableName = "LeftFootCurve";
    public string rightFootAnimVariableName = "RightFootCurve";

    public bool useProIkFeature = false;
    public bool showSolverDebug = true;



    private Animator anim;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    #region FeetGrouding
    private void FixedUpdate()
    {
        if(enableFeetIk == false)
        {
            return;
        }

        if(anim == null)
        {
            return;
        }

        AdjustFeetTarget(ref rightFootIkPosition, HumanBodyBones.RightFoot);
        AdjustFeetTarget(ref leftFootIkPosition, HumanBodyBones.LeftFoot);


    }

    private void OnAnimatorIK(int layerIndex)
    {
        
    }
    #endregion
    #region FeetGroundingMethods
    void MoveFeetToIkPoint(AvatarIKGoal foot, Vector3 positionIkHolder, Quaternion rotationIkHolder, ref float lastFootPositionY)
    {

    }

    private void MovePelvisHeight()
    {

    }

    private void FeetPositionSolver(Vector3 fromSkyPosition, ref Vector3 feetIkPositions, ref Quaternion feetIkRotations)
    {

    }

    private void AdjustFeetTarget(ref Vector3 feetPositons, HumanBodyBones foot)
    {

    }
    #endregion
}
