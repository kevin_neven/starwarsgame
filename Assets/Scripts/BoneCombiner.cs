﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;

public class BoneCombiner
{
    public readonly Dictionary<int, Transform> _rootBoneDictionary = new Dictionary<int, Transform>();
    private readonly Transform[] _boneTransforms = new Transform[36];

    private readonly Transform _transform;

    public BoneCombiner(GameObject rootObj)
    {

        _transform = rootObj.transform;
        traverseHierachy(_transform);
    }

    public Transform AddLimb(GameObject bonedObj, List<string> boneNames)
    {


        var limb = ProcessBonedObject(bonedObj, boneNames);

        limb.SetParent(_transform);
        return limb;
    }

    private Transform ProcessBonedObject(GameObject proccessingLimb, List<string> boneNames)
    {
        var bonedObject =  GameObject.Instantiate(proccessingLimb).transform;
        var meshRenderer = bonedObject.gameObject.AddComponent<SkinnedMeshRenderer>();


        for (var i = 0; i < boneNames.Count; i++)
        {
            _boneTransforms[i] = _rootBoneDictionary[boneNames[i].GetHashCode()];
        }

        var spriteSkins = proccessingLimb.GetComponentsInChildren<SpriteSkin>();


        foreach (var spriteSkin in spriteSkins)
        {
            //spriteSkin.
            var transforms = spriteSkin.boneTransforms;

            foreach (var transform in transforms)
            {
                //Debug.Log(transform.name);
            }
        }

        return bonedObject;



        //meshRenderer.bones = _boneTransforms;
        //meshRenderer.sharedMesh = renderer.sharedMesh;
        //meshRenderer.materials = renderer.sharedMaterials;
    }



    private void traverseHierachy(Transform transform)
    {
        foreach(Transform child in transform)
        {
            _rootBoneDictionary.Add(child.name.GetHashCode(), child);

            traverseHierachy(child);
        }
    }
}
