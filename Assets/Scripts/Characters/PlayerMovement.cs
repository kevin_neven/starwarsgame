﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;
    
    
    
    

    private float doubleTapTime;
    private bool doubleTap;

    bool jumpUp = false;

    [SerializeField]
    float fCutJumpHeight = 0.5f;

    private CharacterInput input = new CharacterInput();
    



    // Start is called before the first frame update
    void Start()
    {
        controller.Init();
    }

    // Update is called once per frame
    void Update()
    {
        input.move = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump"))
        {
            input.jump = true;
        }

        input.holdJump = Input.GetButton("Jump");


        if (Input.GetButtonUp("Jump"))
        {
            jumpUp = true;
        }
        else
        {
            jumpUp = false;
        }
        

        if (Input.GetButtonDown("Crouch"))
        {
            input.crouch = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            input.crouch = false;
        }
        input.shoot = Input.GetButton("Fire1");
        


        if (Input.GetButtonDown("Jump") && doubleTap)
        {
            if (Time.time - doubleTapTime < 0.4f)
            {
                doubleTapTime = 0f;

                input.useJetpack = !input.useJetpack;
            }
            doubleTap = false;
        }

        if (Input.GetButtonDown("Jump") && !doubleTap)
        {
            doubleTap = true;
            doubleTapTime = Time.time;
        }

        input.positionAim = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        controller.SetInput(input);
    }

    // Put physics here
    private void FixedUpdate()
    {
        if (jumpUp && controller.m_Rigidbody2D.velocity.y > 0)
        {
            controller.m_Rigidbody2D.velocity = new Vector2(controller.m_Rigidbody2D.velocity.x, controller.m_Rigidbody2D.velocity.y * fCutJumpHeight);
        }

        // Move our character
        controller.Move();


        
    }

}
