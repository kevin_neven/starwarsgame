﻿using UnityEngine;
using System.Collections;

public class JetpackController : MonoBehaviour
{
    // Float a rigidbody object a set distance above a surface.

    public float floatHeight;     // Desired floating height.
    public float liftForce;       // Force to apply when lifting the rigidbody.
    public float damping;         // Force reduction proportional to speed (reduces bouncing).

    [System.NonSerialized]
    public bool useJetpack = false;
    bool holdJump;
    public LayerMask hoverOnLayer;
    

    public Rigidbody2D rb2D;
    private Vector3 m_Velocity = Vector3.zero;
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement

    [SerializeField] private float flyForce = 10f;

    [System.NonSerialized]
    public CharacterAnimationEvents characterAnimationEvents;

    [System.NonSerialized]
    public AudioSource jetpackSoundEffect;


    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    public void SetInput(CharacterInput input)
    {
        useJetpack = input.useJetpack;
        holdJump = input.holdJump;
    }

    public void Move()
    {
        if (useJetpack)
        {
            characterAnimationEvents.OnJetpack(true);

            if (!jetpackSoundEffect.isPlaying)
            {
                jetpackSoundEffect.Play();
            }


            if (holdJump)
            {
                Vector3 targetVelocity = new Vector2(rb2D.velocity.x, flyForce);
                rb2D.velocity = Vector3.SmoothDamp(rb2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            }
            else
            {

                // Cast a ray straight down.
                RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 100, hoverOnLayer);

                // If it hits something...
                if (hit.collider != null && hit.distance <= floatHeight)
                {

                    // Calculate the distance from the surface and the "error" relative
                    // to the floating height.
                    float distance = Mathf.Abs(hit.point.y - transform.position.y);
                    float heightError = floatHeight - distance;

                    // The force is proportional to the height error, but we remove a part of it
                    // according to the object's speed.
                    float force = liftForce * heightError - rb2D.velocity.y * damping;

                    // Apply the force to the rigidbody.
                    rb2D.AddForce(Vector3.up * force);
                }
            }

        }
        else
        {
            jetpackSoundEffect.Stop();
            characterAnimationEvents.OnJetpack(false);
        }
    }
}