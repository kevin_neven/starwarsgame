﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Animations.Rigging;

public class WeaponController : MonoBehaviour
{

    [System.NonSerialized]
    public CharacterAnimationEvents characterAnimationEvents;



    private GameObject leftHandObject;
    private GameObject rightHandObject;

    [System.NonSerialized]
    public Transform leftHandTransform;
    [System.NonSerialized]
    public Transform rightHandTransform;



    private Transform leftHandFirePoint;
    private GameObject leftHandBulletPrefab;
    private AudioClip[] leftHandFireSounds;
    private AudioSource leftHandAudioSource;

    private Transform rightHandFirePoint;
    private GameObject rightHandBulletPrefab;
    private AudioClip[] rightHandFireSounds;
    private AudioSource rightHandAudioSource;

    [System.NonSerialized]
    public InventoryObject equipment;

    [System.NonSerialized]
    public MultiAimConstraint headAimConstraint;
    [System.NonSerialized]
    public OverrideTransform leftArmAimConstraint;
    [System.NonSerialized]
    public OverrideTransform rightArmAimConstraint;



    // Shooting limmitation
    public float fireDelta = 0.25F;
    private float nextFire = 0.5F;
    private float myTime = 0.0F;








    private bool shoot;
    
    

    public void Init()
    {

        // By default the character should not be aiming
        SetAiming(BodyParts.RightHand, false);
        SetAiming(BodyParts.LeftHand, false);

        for (int i = 0; i < equipment.GetSlots.Length; i++)
        {

            InventorySlot slot = equipment.GetSlots[i];

            if (slot.bodyPart == BodyParts.RightHand || slot.bodyPart == BodyParts.LeftHand)
            {
                slot.OnBeforeUpdate += OnRemoveItem;
                slot.OnAfterUpdate += OnAdditem;
            }
        }

    }

    public void SetInput(CharacterInput input)
    {
        shoot = input.shoot;

        Debug.Log(shoot);
    }

    public void OnRemoveItem(InventorySlot _slot)
    {
        if (_slot.ItemObject == null)
            return;

        if(_slot.bodyPart == BodyParts.LeftHand)
        {
            Destroy(leftHandObject);
        }
        else if (_slot.bodyPart == BodyParts.RightHand)
        {
            Destroy(rightHandObject);
        }
        // If the character is not equiped with a weapon in this hand stop aiming this hand
        SetAiming(_slot.bodyPart, false);
    }

    public void OnAdditem(InventorySlot _slot)
    {


        if (_slot.ItemObject == null)
            return;

        if (_slot.ItemObject.characterDisplay == null)
            return;

        // If the character is equiped with a weapon stop aiming
        SetAiming(_slot.bodyPart, true);
        
        


        InstantiateWeapon((ItemGunObject)_slot.ItemObject, _slot.bodyPart);
    }

    private void InstantiateWeapon(ItemGunObject itemObject, BodyParts bodyPart)
    {
        if (bodyPart == BodyParts.LeftHand)
        {
            leftHandObject = Instantiate(itemObject.characterDisplay, leftHandTransform.position, leftHandTransform.rotation, leftHandTransform.transform);
            leftHandObject.GetComponent<SpriteRenderer>().sortingOrder = 5;
            if(itemObject is ItemGunObject)
            {
                leftHandFireSounds = itemObject.fireSounds;
                leftHandBulletPrefab = itemObject.bulletPrefab;
            }
            leftHandFirePoint = leftHandObject.transform.Find("Fire Point");
            leftHandAudioSource = leftHandObject.transform.Find("AudioSource").GetComponent<AudioSource>();
        }
        else
        {
            rightHandObject = Instantiate(itemObject.characterDisplay, rightHandTransform.position, rightHandTransform.rotation, rightHandTransform.transform);
            rightHandObject.GetComponent<SpriteRenderer>().sortingOrder = 24;
            rightHandFireSounds = itemObject.fireSounds;
            rightHandBulletPrefab = itemObject.bulletPrefab;
            rightHandFirePoint = rightHandObject.transform.Find("Fire Point");
            rightHandAudioSource = rightHandObject.transform.Find("AudioSource").GetComponent<AudioSource>();
        }
    }

    private BodyParts keyLastShot = BodyParts.None;

    // Update is called once per frame
    void Update()
    {
        myTime = myTime + Time.deltaTime;


        if (shoot && myTime > nextFire)
        {

            nextFire = myTime + fireDelta;



            Debug.Log(leftHandObject);
            if (leftHandObject && rightHandObject)
            {
                if (keyLastShot == BodyParts.None || keyLastShot == BodyParts.LeftHand)
                {
                    Shoot(BodyParts.RightHand);
                }
                else
                {
                    Shoot(BodyParts.LeftHand);
                }
            }
            else if(leftHandObject)
            {
                Shoot(BodyParts.LeftHand);
            } else if(rightHandObject)
            {
                Shoot(BodyParts.RightHand);
            }


            nextFire = nextFire - myTime;
            myTime = 0.0F;
        }
    }

    public void Shoot(BodyParts bodyPart)
    {



        // Store variables required for shooting each hand in temp variables so we only have to write the executing code once
        AudioClip[] fireSounds = null;
        Transform firePoint = null;
        GameObject bulletPrefab = null;
        AudioSource audioSource = null;

        if (bodyPart == BodyParts.LeftHand)
        {
            characterAnimationEvents.OnShootingLeft();
            fireSounds = leftHandFireSounds;
            firePoint = leftHandFirePoint;
            bulletPrefab = leftHandBulletPrefab;
            audioSource = leftHandAudioSource;
        }
        else
        {
            characterAnimationEvents.OnShootingRight();
            fireSounds = rightHandFireSounds;
            firePoint = rightHandFirePoint;
            bulletPrefab = rightHandBulletPrefab;
            audioSource = rightHandAudioSource;
        }

        keyLastShot = bodyPart;
        
        int randomNumber = UnityEngine.Random.Range(0, fireSounds.Length);
        audioSource.PlayOneShot(fireSounds[randomNumber]);
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation).GetComponent<Bullet>().shotBy = gameObject;
    }


    private void SetAiming(BodyParts bodyPart, bool on)
    {

        if (on)
        {
            if(bodyPart == BodyParts.LeftHand)
            {
                leftArmAimConstraint.weight = 1;
                characterAnimationEvents.SetAimingLeft(true);
            }
            if(bodyPart == BodyParts.RightHand)
            {
                rightArmAimConstraint.weight = 1;
                characterAnimationEvents.SetAimingRight(true);
            }
        }
        else
        {
            if (bodyPart == BodyParts.LeftHand)
            {
                leftArmAimConstraint.weight = 0;
                characterAnimationEvents.SetAimingLeft(false);
            }
            if (bodyPart == BodyParts.RightHand)
            {
                rightArmAimConstraint.weight = 0;
                characterAnimationEvents.SetAimingRight(false);
            }
        }

        headAimConstraint.weight = 1;
    }
}
