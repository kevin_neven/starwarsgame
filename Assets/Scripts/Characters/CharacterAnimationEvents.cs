﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationEvents : MonoBehaviour
{
    public Animator animator;
    public CharacterController controller;

    public void OnLanding(bool foo)
    {
    }

    public void OnCrouching(bool isCrouching)
    {
        if (animator)
            animator.SetBool("IsCrouching", isCrouching);
    }

    public void OnJumping()
    {
        if (animator)
            animator.SetTrigger("Jump");
    }

    public void JumpEvent()
    {
        controller.m_Rigidbody2D.velocity = new Vector2(controller.m_Rigidbody2D.velocity.x, Mathf.Sqrt(-2.0f * (Physics2D.gravity.y * controller.m_Rigidbody2D.gravityScale) * controller.character.jumpHeight));
    }

    public void OnFall(bool falling)
    {
        if (animator)
            animator.SetBool("Falling", falling);
    }

    public void OnJetpack(bool jetpack)
    {
        if (animator)
            animator.SetBool("Jetpack", jetpack);
    }

    public void OnStep()
    {

    }

    public void SetSpeed(float speed)
    {
        if (animator)
            animator.SetFloat("Speed", speed);
    }

    public void SetAimingLeft(bool aiming)
    {
        if (animator)
            animator.SetBool("Pistol Aim Left", aiming);
    }

    public void SetAimingRight(bool aiming)
    {
        if (animator)
            animator.SetBool("Pistol Aim Right", aiming);
    }

    public void OnShootingLeft()
    {
        if (animator)
            animator.SetTrigger("Pistol Shoot Left Hand");
    }

    public void OnShootingRight()
    {
        if (animator)
            animator.SetTrigger("Pistol Shoot Right Hand");
    }
}
