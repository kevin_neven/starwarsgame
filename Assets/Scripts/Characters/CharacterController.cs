﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using UnityEngine.Events;


[System.Serializable]
public class BoolEvent : UnityEvent<bool> { }

public class CharacterController : MonoBehaviour, ISerializationCallbackReceiver
{
    // Controllers

    private JetpackController jetpackController;
    private WeaponController weaponController;


    // Jumping

    float fJumpPressedRemember = 0;
    [SerializeField]
    float fJumpPressedRememberTime = 0.2f;

    float fGroundRemember = 0;
    [SerializeField]
    float fGroundRememberTime = 0.2f;



    // Information stored about the character
    public CharacterObject character;

    public EquipmentObject equipment;
    public InventoryObject inventory;

    // Variables to store current values

    private GameObject graphics;
    public float health;
    private float speed;


    CharacterInput input;


    [System.NonSerialized]
    public HealthUpdated OnAfterHealthUpdate;
    [System.NonSerialized]
    public HealthUpdated OnBeforeHealthUpdate;


    private CharacterAnimationEvents characterAnimationEvents;
    
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
    [SerializeField] private BoxCollider2D boxCollider2d;                // A collider that will be disabled when crouching

    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    public Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;
    

    private bool m_wasCrouching = false;

    public bool grounded;
    float move;
    bool crouch;
    bool jump;
    bool holdJump;
    bool shoot;
    Vector3 positionAim;

    

    public float FallVelocity;
    





    private Transform headAimTarget;
    private Transform leftArmTarget;
    private Transform rightArmTarget;


    private void Awake()
    {

        m_Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    public void Init()
    {
        // Initialize variables
        jetpackController = GetComponent<JetpackController>();
        weaponController = GetComponent<WeaponController>();

        graphics = Instantiate(character.graphics, transform);
        graphics.name = "Graphics";

        headAimTarget = graphics.transform.Find("Rig 1").Find("Head Aim").Find("Target Head");
        leftArmTarget = graphics.transform.Find("Rig 1").Find("Left Arm").Find("Target Left Arm");
        rightArmTarget = graphics.transform.Find("Rig 1").Find("Right Arm").Find("Target Right Arm");



        characterAnimationEvents = graphics.GetComponent<CharacterAnimationEvents>();
        characterAnimationEvents.controller = this;
        characterAnimationEvents.animator = graphics.GetComponent<Animator>();
        jetpackController.characterAnimationEvents = characterAnimationEvents;
        weaponController.characterAnimationEvents = characterAnimationEvents;


        weaponController.headAimConstraint = graphics.transform.Find("Rig 1").Find("Head Aim").GetComponent<MultiAimConstraint>();
        weaponController.leftArmAimConstraint = graphics.transform.Find("Rig 1").Find("Left Arm").GetComponent<OverrideTransform>();
        weaponController.rightArmAimConstraint = graphics.transform.Find("Rig 1").Find("Right Arm").GetComponent<OverrideTransform>();




        weaponController.leftHandTransform = graphics.transform.Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_8").Find("bone_9").Find("bone_35").Find("LeftHandTransform");
        weaponController.rightHandTransform = graphics.transform.Find("bone_1").Find("bone_2").Find("bone_3").Find("bone_4").Find("bone_5").Find("bone_36").Find("RightHandTransform");

        weaponController.equipment = equipment;
        weaponController.Init();

        jetpackController.jetpackSoundEffect = transform.Find("JetpackSoundEffect").GetComponent<AudioSource>();

        for (int i = 0; i < character.defaultEquipment.GetSlots.Length; i++)
        {
            InventorySlot defaultSlot = character.defaultEquipment.GetSlots[i];
            for (int j = 0; j < equipment.GetSlots.Length; j++)
            {
                InventorySlot equipmentSlot = equipment.GetSlots[j];
                if (defaultSlot.item.Id != -1 && equipmentSlot.bodyPart == defaultSlot.bodyPart)
                {
                    equipment.GetSlots[j].UpdateSlot(defaultSlot.item, defaultSlot.amount);
                }
            }
        }




        health = character.health;
        speed = character.speed;


        StartCoroutine(addHealth());
    }

    IEnumerator addHealth()
    {
        while (true)
        { // loops forever...
            if (health < character.health)
            { // if health < 100...
                if(OnBeforeHealthUpdate != null)
                    OnBeforeHealthUpdate.Invoke(health);
                health += 0.1f; // increase health and wait the specified time
                if (OnAfterHealthUpdate != null)
                    OnAfterHealthUpdate.Invoke(health);
                yield return new WaitForSeconds(0.05f);
            }
            else
            { // if health >= 100, just yield 
                yield return null;
            }
        }
    }

    private void FixedUpdate()
    {
        characterAnimationEvents.OnLanding(!IsGrounded() && !jetpackController.useJetpack && m_Rigidbody2D.velocity.y < FallVelocity);
    }
    public void SetInput(CharacterInput _input)
    {
        input = _input;
        move = input.move;
        crouch = input.crouch;
        jump = input.jump;
        positionAim = input.positionAim;



        jetpackController.SetInput(input);
        weaponController.SetInput(input);
    }

    public void SetInputAttack(bool _shoot)
    {
        shoot = _shoot;
    }
    
    public void Move()
    {
        // If crouching, check to see if the character can stand up
        if (!crouch)
        {
            // If the character has a ceiling preventing them from standing up, keep them crouching
            if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
            {
                // crouch = true;
            }
        }

        //only control the player if grounded or airControl is turned on
        if (IsGrounded() || m_AirControl)
        {

            // If crouching
            if (crouch)
            {
                if (!m_wasCrouching)
                {
                    m_wasCrouching = true;
                    characterAnimationEvents.OnCrouching(true);
                }

                // Reduce the speed by the crouchSpeed multiplier
                move *= m_CrouchSpeed;

                // Disable one of the colliders when crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = false;
            }
            else
            {
                // Enable the collider when not crouching
                if (m_CrouchDisableCollider != null)
                    m_CrouchDisableCollider.enabled = true;

                if (m_wasCrouching)
                {
                    m_wasCrouching = false;
                    characterAnimationEvents.OnCrouching(false);
                }
            }

            // Move the character by finding the target velocity
            Vector3 targetVelocity = new Vector2(move * Time.fixedDeltaTime * speed, m_Rigidbody2D.velocity.y);
            // And then smoothing it out and applying it to the character
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

                

            

            Vector3 newPosition = new Vector3(positionAim.x, positionAim.y);
            headAimTarget.position = newPosition;
            leftArmTarget.position = newPosition;
            rightArmTarget.position = newPosition;


            transform.rotation = Quaternion.Euler(new Vector3(0,  Mathf.Sign(newPosition.x - transform.position.x) == 1 ? 0 : 180, 0));
        }

        // If the player should jump...
        //if (m_Grounded && jump)
        //{
        // Add a vertical force to the player.
        //    m_Grounded = false;
        //    m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
        //}

        // CUSTOM
        // If the player should jump...
        fGroundRemember -= Time.fixedDeltaTime;
        if (IsGrounded())
        {
            fGroundRemember = fGroundRememberTime;
        }

        fJumpPressedRemember -= Time.fixedDeltaTime;
        if (jump)
        {
            fJumpPressedRemember = fJumpPressedRememberTime;
        }

        if (fGroundRemember > 0 && fJumpPressedRemember > 0)
        {
            fJumpPressedRemember = 0;
            fGroundRemember = 0;
            // Add a vertical force to the player.
            // m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            

            characterAnimationEvents.OnJumping();
            input.jump = false;

        }
        characterAnimationEvents.SetSpeed(Mathf.Abs(move) * (speed / 10f));

        //if (jumpDown && IsGrounded())
        //{
        //    fJumpPressedRemember = 0;
        //    fGroundRemember = 0;
        //    // Add a vertical force to the player.
        //    m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));

        //    grounded = false;
        //    OnJumpEvent.Invoke();
        //}


        jetpackController.Move();
    }

    private bool IsGrounded()
    {
        float extraHeight = 0.15f;
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size, 0f, Vector2.down, extraHeight, m_WhatIsGround);
        Color raycolor;
        if(raycastHit.collider != null)
        {
            raycolor = Color.green;
        }
        else
        {
            raycolor = Color.red;
        }
        Debug.DrawRay(boxCollider2d.bounds.center + new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeight), raycolor);
        Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, 0), Vector2.down * (boxCollider2d.bounds.extents.y + extraHeight), raycolor);
        Debug.DrawRay(boxCollider2d.bounds.center - new Vector3(boxCollider2d.bounds.extents.x, boxCollider2d.bounds.extents.y), Vector2.right * (boxCollider2d.bounds.extents.x), raycolor);
        return raycastHit.collider != null;
    }



    void Die()
    {
        Instantiate(character.deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }



    public void TakeDamage(int damage)
    {
        if(OnBeforeHealthUpdate != null)
            OnBeforeHealthUpdate.Invoke(health);
        health -= damage;
        if (OnAfterHealthUpdate != null)
            OnAfterHealthUpdate.Invoke(health);

        if (health <= 0)
        {
            Die();
        }
    }

    public void OnBeforeSerialize()
    {
        //if (transform.Find("Graphics") != null)
        //    DestroyImmediate(transform.Find("Graphics").gameObject);

        //graphics = Instantiate(character.graphics, transform);
        //graphics.name = "Graphics";
    }

    public void OnAfterDeserialize()
    {
    }
}


public delegate void HealthUpdated(float _health);

public class CharacterInput
{
    public float move;
    public bool crouch;
    public bool jump;
    public bool holdJump;
    public bool useJetpack;
    public Vector3 positionAim;

    public bool shoot;
}