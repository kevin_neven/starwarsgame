﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;
using UnityEditor;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{


    /**
     * raycast voor bepalen of ai kan springen

character moet een max jump height hebben

doe een for loop over de max jump height met een distance

spring de hoogte van de eerste ray die niets raakt 
    */



    private CharacterController controller;

    private CharacterInput input = new CharacterInput();

    private float health;

    private Transform lookAtTarget;
    public Transform target;
    private float speed;
    public float nextWaypointDistance = 3f;

    Path path;
    int currentWaypoint = 0;
    bool reachedEndOfPath = false;

    Seeker seeker;
    Rigidbody2D rb;

    Vector2 direction;

    float totalDistance = 0;

    public float jumpOverHeight = 0.75f;


    float randomChance;

    private void Start()
    {
        // Get required components for physics, moving and ai
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();
        controller = GetComponent<CharacterController>();

        EquipmentObject equipment = new EquipmentObject();
        equipment.InitEquipment();
        equipment.InitDatabase();
        controller.equipment = equipment;


        InventoryObject inventory = new InventoryObject();
        inventory.InitInventory();
        inventory.InitDatabase();
        controller.inventory = inventory;


        controller.Init();

        lookAtTarget = target.Find("Graphics").Find("bone_1").Find("bone_2").Find("bone_3").Find("Target_AI");

        // Update the path every 0.5 seconds
        InvokeRepeating("UpdatePath", 0f, 0.5f);
        InvokeRepeating("RandomChance", 0f, 0.25f);
    }

    void UpdatePath()
    {
        // Only update the path if we aren't trying to do so already
        if(seeker.IsDone() && target != null)
            seeker.StartPath(rb.position, target.position, OnPathComplete);
    }

    void RandomChance()
    {
        randomChance = Random.value;
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
            return;
        }
        Debug.Log(p.error);
    }

    private void FixedUpdate()
    {
        if (path == null)
            return;

        if(currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;

        totalDistance = 0;
        for (int i = 0; i < path.vectorPath.Count; i++)
        {
            totalDistance += Vector2.Distance(rb.position, path.vectorPath[i]);
        }


        // Vector2 force = new Vector2(direction.x, 0f) * speed * Time.deltaTime;
        LookDirection();
        Walking();
        Jumping();
        Shooting();

        controller.SetInput(input);
        controller.Move();

        // rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        if(distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }

        //if(force.x >= 0.01f)
        //{
        //    // graphics.localScale = new Vector3(1f, 1f, 1f);
        //}
        //else if(force.x <= -0.01f)
        //{
        //    // graphics.localScale = new Vector3(-1f, 1f, 1f);
        //}


    }

    void LookDirection()
    {
        if (lookAtTarget)
            input.positionAim = lookAtTarget.position;
    }

    void Walking()
    {
        Debug.Log(totalDistance);
        if(totalDistance >= 20f)
        {
            input.move = direction.x < 0 ? -1 : 1;
        }
        else
        {
            input.move = 0;
        }
    }

    void Shooting()
    {
        bool willShoot = true;

        if (totalDistance >= 30f)
        {
            willShoot = false;
        }

        if(target == null)
        {
            willShoot = false;
        }

        if (randomChance < .45f)
            willShoot = false;

        input.shoot = willShoot;
    }


    void Jumping()
    {
        if (direction.y <= 0)
            return;
        

        float jumpHeight = controller.character.jumpHeight;

        float heightCheck = 1f;
        float distanceJump = 1f;

        Collider2D collider = null;

        for (float i = heightCheck; i <= jumpHeight; i += heightCheck)
        {
            RaycastHit2D raycastHit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + i), new Vector2(input.move, 0), distanceJump, LayerMask.GetMask("Ground"));
            if (raycastHit.collider != null)
            {
                collider = raycastHit.collider;
            } else
            {
                break;
            }
        }

        //RaycastHit2D obstcaleNear = Physics2D.Raycast(transform.position, new Vector2(input.move, 0));

        if (collider != null)
        {
            input.jump = true;
            
        }
    }
}
