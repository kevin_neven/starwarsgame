﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject shotBy;

    public float speed = 20f;
    public int damage = 40;
    public Rigidbody2D rb;
    public GameObject impactEffect;

    public float destroyAfterTime = 10f;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
        Destroy(gameObject, destroyAfterTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == shotBy || collision.GetComponent<Bullet>() != null)
            return;

        CharacterController enemy = collision.GetComponent<CharacterController>();

        if(enemy != null)
        {
            enemy.TakeDamage(damage);
        }

        


        Instantiate(impactEffect, collision.ClosestPoint(transform.position), transform.rotation);
        Destroy(gameObject);

        Destroy(gameObject);
    }
}
