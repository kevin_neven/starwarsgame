﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Healthbar : MonoBehaviour
{
    public Slider healthSlider;

    public CharacterController controller;

    

    // Update is called once per frame
    void Update()
    {
        if (!controller)
            return;
        controller.OnAfterHealthUpdate += UpdateHealth;
    }

    void UpdateHealth(float health)
    {
        healthSlider.value = health * (healthSlider.maxValue / controller.character.health);
    }
}
