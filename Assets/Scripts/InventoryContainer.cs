﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryContainer : MonoBehaviour
{
    public List<GameObject> screens;

    private bool _open;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            _open = !_open;
        }

        foreach (var screen in screens)
        {
            screen.active = _open;
        }
    }
}
