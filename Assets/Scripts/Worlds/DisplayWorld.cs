﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class DisplayWorld : MonoBehaviour
{
    public WorldObject worldObject;
    public GameObject groundPrefab;

    private void Start()
    {

        worldObject.Container.Generate();

        foreach (Chunk chunk in worldObject.GetChunks)
        {
            DisplayChunk(chunk);
        }
    }


    public void DisplayChunk(Chunk chunk)
    {
        BuildGround(chunk.ground);
    }

    public void BuildGround(Ground ground)
    {
        GameObject groundObject = Instantiate(groundPrefab);

        SpriteShapeController spriteShapeController = groundObject.GetComponent<SpriteShapeController>();
        EdgeCollider2D edgeCollider2D = groundObject.GetComponent<EdgeCollider2D>();


        Spline spline = spriteShapeController.spline;
        spline.Clear();
        
        for (int i = 0; i < ground.verticies.Count; i++)
        {
            spline.InsertPointAt(i, ground.verticies[i]);
        }

        List<Vector2> closedShape = new List<Vector2>();
        closedShape.AddRange(ground.verticies);
        closedShape.Add(ground.verticies[0]);

        edgeCollider2D.points = closedShape.ToArray();


        spriteShapeController.RefreshSpriteShape();

        spriteShapeController.BakeCollider();
        spriteShapeController.BakeMesh().Complete();
        
    }
}
