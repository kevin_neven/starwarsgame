﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class GenerateWorld : MonoBehaviour
{
    public float tangentLength = 1.0f;
    [SerializeField]
    private GameObject groundPrefab;
    public SpriteShapeController spriteShapeController;
    public EdgeCollider2D edgeCollider2D;

    public List<Vector2> newVerticies = new List<Vector2>();
    float seed;
    public float smoothness;
    public float heightMultiplier;
    public int heightAddition;

    public int chunkWidth = 20;
    public int numChunks = 5;
    private int lastX = 0;


    public void Update()
    {
    }

    public void Start()
    {
        seed = Random.Range(-100000f, 100000f);
        GenerateChunks();

    }

    void SetSpline()
    {
    }

    public void GenerateChunks()
    {
        for (int i = 0; i < numChunks; i++)
        {
            GenerateChunk();
        }
    }

    private void GenerateChunk()
    {

        lastX += chunkWidth;
    }
}


//public class Ground
//{
//    public SpriteShapeController spriteShapeController;
//    public EdgeCollider2D edgeCollider2D;

//    public List<Vector2> verticies = new List<Vector2>();
//    private int seed;

//    public void Generate(int _seed)
//    {
//        seed = _seed;

//        Spline spline = spriteShapeController.spline;
//        spline.Clear();
//        verticies = new List<Vector2>();


//        spline.InsertPointAt(0, new Vector3(0, 0));
//        verticies.Add(new Vector3(0, 0));

//        for (int i = lastX; i <= lastX + chunkWidth; i++)
//        {
//            int h = Mathf.RoundToInt(Mathf.PerlinNoise(seed, (i + transform.position.x) / smoothness) * heightMultiplier) + heightAddition;
//            Vector2 position = new Vector2(i, h);
//            newVerticies.Add(position);
//            spline.InsertPointAt(i + 1, position);

//            Quaternion rotation = transform.rotation;

//            spline.SetTangentMode(i, ShapeTangentMode.Continuous);
//            spline.SetRightTangent(i, rotation * Vector3.down * tangentLength);
//            spline.SetLeftTangent(i, rotation * Vector3.up * tangentLength);
//        }
//        spline.InsertPointAt(chunkWidth + 2, new Vector2(lastX + chunkWidth, 0));
//        newVerticies.Add(new Vector3(lastX + chunkWidth, 0));
//        newVerticies.Add(new Vector3(0, 0));
//        edgeCollider2D.points = newVerticies.ToArray();


//        spriteShapeController.RefreshSpriteShape();

//        spriteShapeController.BakeCollider();
//        spriteShapeController.BakeMesh().Complete();
//    }
//}