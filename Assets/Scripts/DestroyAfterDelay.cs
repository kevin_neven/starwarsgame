﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class DestroyAfterDelay : MonoBehaviour
{
    public float destroyTime = 1f;
    public float disableTime = 1f;
    private VisualEffect visualEffect;

    // Start is called before the first frame update
    void Start()
    {
        visualEffect = GetComponent<VisualEffect>();

        if (visualEffect != null)
            StartCoroutine(Disable());

    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, destroyTime);
    }

    IEnumerator Disable()
    {
        yield return new WaitForSeconds(disableTime);
        visualEffect.Stop();
    }
}
