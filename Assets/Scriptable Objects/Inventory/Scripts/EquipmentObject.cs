﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment", menuName = "Inventory System/Equipment")]
public class EquipmentObject : InventoryObject
{
    [ContextMenu("InitEquipment")]
    public void InitEquipment ()
    {
        type = InterfaceType.Equipment;

        InventorySlot helmetSlot = new InventorySlot
        {
            AllowedItems = new ItemType[] { ItemType.Helmet },
            bodyPart = BodyParts.Helmet
        };

        InventorySlot rightHandSlot = new InventorySlot
        {
            AllowedItems = new ItemType[] { ItemType.Weapon },
            bodyPart = BodyParts.RightHand
        };

        InventorySlot leftHandSlot = new InventorySlot
        {
            AllowedItems = new ItemType[] { ItemType.Weapon, ItemType.Shield },
            bodyPart = BodyParts.LeftHand
        };
            
        InventorySlot chestSlot = new InventorySlot
        {
            AllowedItems = new ItemType[] { ItemType.Chest },
            bodyPart = BodyParts.Chest
        };

        InventorySlot bootsSlot = new InventorySlot
        {
            AllowedItems = new ItemType[] { ItemType.Boots },
            bodyPart = BodyParts.Boots
        };


        Container = new Inventory();
        Container.Slots = new InventorySlot[] { helmetSlot, rightHandSlot, leftHandSlot, chestSlot, bootsSlot };


    }
}
