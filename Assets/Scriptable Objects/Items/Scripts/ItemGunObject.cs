﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory System/Items/Gun")]
public class ItemGunObject : ItemObject
{
    public AudioClip[] fireSounds;
    public GameObject bulletPrefab;

    public int clipSize;
    public int maxAmo;

}

[System.Serializable]
public class GunItem : Item
{
    public int amo;
}