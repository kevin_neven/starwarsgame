﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun Item Database", menuName = "Inventory System/Items/Gun Database")]
public class GunDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
{
    public ItemGunObject[] GunItemObjects;

    [ContextMenu("Update ID's")]
    public void UpdateID()
    {
        for (int i = 0; i < GunItemObjects.Length; i++)
        {
            if (GunItemObjects[i].data.Id != i)
                GunItemObjects[i].data.Id = i;
        }
    }
    public void OnAfterDeserialize()
    {
        UpdateID();
    }

    public void OnBeforeSerialize()
    {
    }
}
