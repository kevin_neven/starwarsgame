﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Chararacter Database", menuName = "Character System/Characters/Database")]
public class CharacterDatabaseObject : ScriptableObject, ISerializationCallbackReceiver
{
    public CharacterObject[] CharacterObjects;

    [ContextMenu("Update ID's")]
    public void UpdateID()
    {
        for (int i = 0; i < CharacterObjects.Length; i++)
        {
            if (CharacterObjects[i].data.Id != i)
                CharacterObjects[i].data.Id = i;
        }
    }
    public void OnAfterDeserialize()
    {
        UpdateID();
    }

    public void OnBeforeSerialize()
    {
    }
}
