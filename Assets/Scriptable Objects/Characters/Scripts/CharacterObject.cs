﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D.Animation;

[CreateAssetMenu(fileName = "New Item", menuName = "Character System/Characters/Character")]
public class CharacterObject : ScriptableObject
{
    public Sprite uiDisplay;
    public GameObject graphics;
    public GameObject deathEffect;

    public AudioClip[] runSoundsDirtLeft;
    public AudioClip[] runSoundsDirtRight;


    public InventoryObject defaultEquipment;

    public float health;
    public float speed;
    public float jumpHeight;

    [TextArea(15, 20)]
    public string description;

    public Character data = new Character();

    public Character CreateCharacter()
    {
        Character newCharacter = new Character(this);
        return newCharacter;
    }


    /// <summary>
    /// I don't know what to do with this method yet tbh. Don't use it
    /// </summary>
    public void OnValidate()
    {
    }


}

[System.Serializable]
public class Character
{
    public string Name;
    public int Id = -1;

    public Attribute[] attributes;
    public Character()
    {
        Name = "";
        Id = -1;
    }
    public Character(CharacterObject character)
    {
        Name = character.name;
        Id = character.data.Id;
    }
}