﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEditor;
using System.Runtime.Serialization;
using UnityEngine.U2D;

[CreateAssetMenu(fileName = "New World", menuName = "World Generation System/World")]
public class WorldObject : ScriptableObject
{
    public string savePath;
    public ItemDatabaseObject database;
    public World Container;
    public List<Chunk> GetChunks { get { return Container.Chunks; } }

    [ContextMenu("Save")]
    public void Save()
    {
        //string saveData = JsonUtility.ToJson(this, true);
        //BinaryFormatter bf = new BinaryFormatter();
        //FileStream file = File.Create(string.Concat(Application.persistentDataPath, savePath));
        //bf.Serialize(file, saveData);
        //file.Close();

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(string.Concat(Application.persistentDataPath, savePath), FileMode.Create, FileAccess.Write);
        formatter.Serialize(stream, Container);
        stream.Close();
    }
    [ContextMenu("Load")]
    public void Load()
    {
        if (File.Exists(string.Concat(Application.persistentDataPath, savePath)))
        {
            //BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(string.Concat(Application.persistentDataPath, savePath), FileMode.Open);
            //JsonUtility.FromJsonOverwrite(bf.Deserialize(file).ToString(), this);
            //file.Close();

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(string.Concat(Application.persistentDataPath, savePath), FileMode.Open, FileAccess.Read);
            World newContainer = (World)formatter.Deserialize(stream);
            

            stream.Close();
        }
    }
    [ContextMenu("Clear")]
    public void Clear()
    {
        Container.Clear();
    }
}
[System.Serializable]
public class World
{
    public float seed;

    public List<Chunk> Chunks = new List<Chunk>();
    [System.NonSerialized]
    public int ChunkWidth = 20;
    [System.NonSerialized]
    public int numChunks = 5;
    [System.NonSerialized]
    public float Smoothness = 10;
    [System.NonSerialized]
    public float HeightMultiplier = 10;
    [System.NonSerialized]
    public float HeightAddition = 1;


    public HeightMap heightMap;

    public int LastX()
    {
        return Chunks.Count * ChunkWidth;
    }

    public void Clear()
    {
        Chunks = new List<Chunk>();
    }

    public void Generate()
    {
        Chunks = new List<Chunk>();
        seed = Random.Range(-100000f, 100000f);

        heightMap = new HeightMap(this);
        heightMap.Generate();

        for (int i = 0; i < numChunks; i++)
        {
            Chunk chunk = new Chunk(this);

            chunk.Generate();

            Chunks.Add(chunk);
        }
    }
}

[System.Serializable]
public class HeightMap
{
    public List<Vector2> verticies = new List<Vector2>();
    private World world;

    public HeightMap(World _world)
    {
        world = _world;
    }

    public void Generate()
    {
        verticies = new List<Vector2>();
        for (int i = 0; i <= world.numChunks * world.ChunkWidth; i++)
        {
            float h = Mathf.PerlinNoise(world.seed, (i + world.ChunkWidth) / world.Smoothness) * world.HeightMultiplier + world.HeightAddition;
            Vector2 position = new Vector2(i, h);
            verticies.Add(position);
        }

    }
}

[System.Serializable]
public class Chunk
{
    [System.NonSerialized]
    public GameObject groundDisplay;
    [System.NonSerialized]
    public SlotUpdated OnAfterUpdate;
    [System.NonSerialized]
    public SlotUpdated OnBeforeUpdate;
    public Ground ground;
    public World world;

    public Chunk(World _world)
    {
        world = _world;
    }

    public void Generate()
    {
        ground = new Ground(world);
        ground.Generate();
    }
}

[System.Serializable]
public class Ground
{
    public List<Vector2> verticies = new List<Vector2>();
    public float tangentLength = 1.0f;
    public World world;

    public Ground(World _world)
    {
        world = _world;
    }

    public void Generate()
    {
        int lastX = world.LastX();
        verticies = new List<Vector2>
        {
            new Vector2(lastX, 0)
        };
        for (int i = lastX; i <= lastX + world.ChunkWidth; i++)
        {
            verticies.Add(world.heightMap.verticies[i]);
        }
        verticies.Add(new Vector2(lastX + world.ChunkWidth, 0));
    }
}